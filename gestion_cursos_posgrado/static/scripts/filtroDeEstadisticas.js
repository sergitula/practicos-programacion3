function ocultarMostrarFiltro(){
    let cb = document.getElementById("filtrarSI");
    let dFiltro = document.getElementById("contenedorFormFiltro");
    let dSin = document.getElementById("contenedorOpcionesSinFiltro");
    if(cb.checked==true){
        dFiltro.style.display = "flex";
        dSin.style.display = "none";
    }
    else{
        dFiltro.style.display = "none";
        dSin.style.display = "flex";
    }
}

function redirigirFiltrarCursos(){
    let formulario = document.getElementById("formFiltro");
    formulario.action = "./estadisticas/estadisticasFiltradas";
}


function redirigirFiltrarCursoInscripciones(){
    let formulario = document.getElementById("formFiltro");
    formulario.action = "./estadisticas/inscriptosALosCursosFiltrados";
}


function redirigirFiltrarCursoPagos(){
    let formulario = document.getElementById("formFiltro");
    formulario.action = "./estadisticas/pagosEnCursosFiltrados";
}