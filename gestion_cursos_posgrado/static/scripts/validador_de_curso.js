
function verCarta(){
	let carta = document.getElementById("carta_p");
	carta.style.display = "flex";
}

function validarPeriodo(){
	let fechaInicio = document.getElementById("fecha_inic");let fechaFinal = document.getElementById("fecha_fin");
	
	if(fechaInicio.value > fechaFinal.value){
		alert("[Error] La fecha de Incio del curso no debe ser posterior a la Fin de curso");
		return false;
	}
	else{
		return true;
	}
}

function validarCantidadesDeAlumnos(){
	let cantMin = document.getElementById("cant_min_a");let cantMax = document.getElementById("cant_max_a");
	if(cantMin.value > cantMax.value){
		alert('[Error] La cantidad minima de alumnos no puede superar la cantidad maxima');
		return false;
	}
	else{
		return true;
	}	
}	
			
function validador(){				
	let validacionFinal = false;
	console.log(validacionFinal);

	if(validarPeriodo() != false){
		validacionFinal = true;
		if(validarCantidadesDeAlumnos() != false){
			validacionFinal = true;
		}
		else{
			validacionFinal = false;
		}
				
	}
	else{
		validacionFinal = false;
	}
//	console.log("if de periodo");
//	console.log(validacionFinal);
	
//	ole.log(validacionFinal);
	
//	validacionFinal = false;
//	console.log(validacionFinal);
	
	return validacionFinal;
}

//Probar que la funcion agregarProfesores cree un option para el select

/* function agregarProfesores(){
	let prof_anotado = document.getElementById("profesor_ag_c");
	let profes_en_lista = document.getElementById("profesores_c");
	let eleccion = document.createElement("option");
	if(prof_anotado.value != ""){
		eleccion.value = prof_anotado.value;
		eleccion.innerHTML = prof_anotado.value;
		profes_en_lista.appendChild(eleccion);
		prof_anotado.value = "";
	}
}  */

/* function quitarProfesores(){
	let profes_en_lista = document.getElementById("profesores_c");
	let anotados = Array.from(profes_en_lista.selectedOptions);
	for(prof of anotados) {
        profes_en_lista.remove(anotados.index);
    }
} */
 

/*
function eleccionMin1 (){
	let profes_en_lista = document.getElementById("profesores_c");
	let vector = profes_en_lista.childNodes;
	if(vector.length < 1){
		alert("Debe ingresar por lo menos un profesor a la lista");
		return false
	}
	else{
		return true;
	}
}  */

function agregarProfesor(){
	if(noPermitirDocenteRep() == true){
		let ape = document.getElementById("apellido_p");
		let nom = document.getElementById("nombre_p");
		let cui = document.getElementById("cuil");
		let corr = document.getElementById("correo");
		let carta = document.getElementById("carta_p");
		let tabla_p = document.getElementById("tabla_profes");
		
		let t_cuerpo = document.getElementById("cuerpo_tabla_p");
		let filas = t_cuerpo.childNodes;
		
		
		let fila = document.createElement("tr");
		
		fila.className += "fila_p";

		let arrayFila = document.getElementsByClassName("fila_p");
		let cant_filas = arrayFila.length;
	//	console.log(cant_filas);
		
		fila.className += " ";
		fila.className += (cant_filas + 1);
		
		let cell1 = document.createElement("td");
		let cell2 = document.createElement("td");
		let cell3 = document.createElement("td");
		
		cell3.className += "col_cuil";
		
		let cell4 = document.createElement("td");
		let cell5 = document.createElement("td");
		cell5.style.backgroundColor = "red";
		
		fila.appendChild(cell1);
		fila.appendChild(cell2);
		fila.appendChild(cell3);
		fila.appendChild(cell4);
		fila.appendChild(cell5);
		
		t_cuerpo.appendChild(fila);
		
		cell1.innerHTML = ape.value;
		cell2.innerHTML = nom.value;
		cell3.innerHTML = cuil.value;
		cell4.innerHTML = corr.value;
		
		//------------------------------------------------------------------------------------
		let btn_quitar = document.createElement("button");
	//	btn_quitar.value = "Quitar";
		btn_quitar.innerHTML = "Quitar";
		btn_quitar.addEventListener("click",function(evt){
			quitarDocente(this);
		}); 

		cell5.appendChild(btn_quitar);
		//-------------------------------------------------------------------------------
	//	cell5.appendChild(d);
		
		
	//	cell5.appendChild(aQ);
		
		carta.style.display = "none";
		ape.value = "";
		nom.value = "";
		cui.value = "";
		corr.value = "";
	}
	else{
		alert("[Error] Ya ha agregado a un docente con el cuit ingresado");
	} 
}

function ocultarCarta(){
	let carta = document.getElementById("carta_p");
	let ape = document.getElementById("apellido_p");
	let nom = document.getElementById("nombre_p");
	let cui = document.getElementById("cuil");
	let corr = document.getElementById("correo");
	carta.style.display = "none";
	ape.value = "";
	nom.value = "";
	cui.value = "";
	corr.value = "";
}


function quitarDocente(objeto){
	//Guardando la referencia del objeto presionado
	let esto = objeto;
	//Obtener las filas los datos de la fila que se va a elimnar
	//var array_fila=filaSeleccionada(_this);
	let t_cuerpo = document.getElementById("cuerpo_tabla_p");
//	$(this).parent().parent().fadeOut("slow",function(){$(this).remove();});
//	$(this).parent.parent().parent().fadeOut("slow",function(){$(this).remove();});
	t_cuerpo.removeChild(esto.parentNode.parentNode);
	
}


function noPermitirDocenteRep(){
	let cui = document.getElementById("cuil").value;
//	let t_cuerpo = document.getElementById("cuerpo_tabla_p");
//	let filas = t_cuerpo.childNodes;
	
//	let arrayFila = document.getElementsByClassName("fila_p");
//	let cant_filas = arrayFila.length;
	
	let arrayFila = document.getElementsByClassName("col_cuil");
	let resultado = true
	if(arrayFila.length != 0){
		let i;
		for(i = 0; i < arrayFila.length; i++){
			if(arrayFila[i].innerHTML == cui){
				resultado = false;
			}
		}
	}
	return resultado;
}

/*
function ingresarFloat(e){
//	console.log("metodo");
	let key = e.keyCode;// || e.which;
	let tecla = String.fromCharCode(key).toString();
//	console.log(key);
//	console.log(tecla);
	
	
	if(key >= 48 && key <= 57){
		return true;
	}
	else{
		if(key == 46 || key == 44){
			return true;
		}
		else{
			alert("[Error] los digitos validos a ingresar en Costo son números o '.' o ','");
			return false;
			//event.stopPropagation()
		}
	}

} */
