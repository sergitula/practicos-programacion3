from django.contrib import admin

# Register your models here.
from apps.models import provincia, localidad

admin.site.register(localidad)

admin.site.register(provincia)
