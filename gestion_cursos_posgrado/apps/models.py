from django.contrib.auth.models import User
from django.db import models
from datetime import date

class localidad(models.Model):
    dom_cod_localidad = models.PositiveIntegerField(primary_key=True)
    dom_descrip_localidad = models.CharField(max_length=100)

class provincia(models.Model):
    dom_cod_provincia = models.PositiveIntegerField(primary_key=True)
    dom_descrip_provincia = models.CharField(max_length=60)


class persona(models.Model):
    GENERO_OPCIONES_SEXO = {
        ('M','Masculino'),
        ('F','Femenino'),
    }
    GENERO_OPCIONES_ESTADOCIVIL={
        ('SOLTERO','Soltero'),
        ('CASADO','Casado'),
        ('VIUDO','Viudo'),
        ('DIVORCIADO','Divorciado'),
    }
    cuit_persona= models.CharField(max_length=11,primary_key=True)
    apellido = models.CharField(max_length=50,null=True)
    nombre=models.CharField(max_length=50, null=True)
    sexo_persona = models.CharField(max_length=1,choices=GENERO_OPCIONES_SEXO, null=True)
    fecha_nac_persona = models.DateField(null=True)
    estado_civil = models.CharField(max_length=10,choices=GENERO_OPCIONES_ESTADOCIVIL, null=True)
    dom_calle = models.CharField(max_length=40,blank=True, null=True)
    dom_nro_calle = models.PositiveIntegerField(blank=True,null=True)
    departamento = models.CharField(max_length=100,blank=True,null=True)
    correo_electronico = models.CharField(max_length=100, blank=True, null=True)
    telefono = models.BigIntegerField(blank=True, null=True)
    dom_cod_localidad = models.ForeignKey(localidad, on_delete=models.PROTECT, null=True)
    dom_cod_provincia = models.ForeignKey(provincia, on_delete=models.CASCADE, null=True)

    User = models.OneToOneField(
        User,null=True,on_delete=models.CASCADE,
        related_name="%(app_label)s_%(class)s_related",
        related_query_name="%(app_label)s_%(class)ss",
    )

    # User = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL, related_name="content_%(class)s")
    def __str__(self):
        return self.nombre


class curso(models.Model):
    GENERO_OPCIONES_MODALIDAD = {
        ('VIRTUAL', 'Virtual'),
        ('PRESENCIAL','Presencial'),
        ('MIXTA','Mixta'),
    }
    GENERO_OPCIONES_ESTADO = {
        ('A', 'Aprobado'),
        ('P', 'Pendiente'),
        ('D', 'Desaprobado'),
    }
    cod_curso = models.AutoField(primary_key=True)
    nombre_curso = models.CharField(max_length=200, null=True)
    fecha_proposicion = models.DateField(default=date.today)
    objetivo_general = models.CharField(max_length=200, null=True)
    modalidad_curso = models.CharField(max_length=50,choices=GENERO_OPCIONES_MODALIDAD,null=True)
    horario_curso = models.PositiveIntegerField(null=True)
    duracion_curso = models.PositiveIntegerField(null=True)
    fecha_ini_curso = models.DateField(null=True)
    fecha_fin_curso = models.DateField(null=True)
    cant_minima_alumnos = models.PositiveIntegerField(null=True)
    cant_maxima_alumnos = models.PositiveIntegerField(null=True)
    costo_curso = models.PositiveIntegerField(null=True)
    estado_curso = models.CharField(max_length=1,choices=GENERO_OPCIONES_ESTADO, default='P')
    expositores = models.ManyToManyField('docente', related_name='expositores', db_table='expositores')

    def __str__(self):
        return self.nombre_curso


class curso_persona(models.Model):
#<<<<<<< HEAD
    cuit_persona = models.ForeignKey(persona,on_delete=models.CASCADE)  #persona
    cod_curso = models.ForeignKey(curso,on_delete=models.CASCADE)   #curso
    fecha_inscripcion = models.DateField(default=date.today)
    fehca_examen = models.DateField(null=True)
    nota_examen = models.SmallIntegerField(null=True)
    porcentaje_asistencia = models.SmallIntegerField(null=True)
    nro_registro = models.AutoField(primary_key=True)
    #nro_intentos = models.SmallIntegerField(primary_key=True)
    estado_de_pago = models.BooleanField(default=False)
"""
=======
    cuit_persona = models.ForeignKey(persona,on_delete=models.CASCADE)
    cod_curso = models.ForeignKey(curso,on_delete=models.CASCADE)
    fecha_inscripcion = models.DateField(default=date.today)
    fehca_examen = models.DateField(null=True,blank=True)
    nota_examen = models.SmallIntegerField(null=True,blank=True)
    porcentaje_asistencia = models.SmallIntegerField(null=True,blank=True)
    nro_registro = models.AutoField(primary_key=True)


>>>>>>> 09aa07856dab98ae8a5a7bf09613b30f3a9463b5 """

# **** RELACION-UNO A UNO- (OneToOneField) ****
class docente(models.Model):
    cuit_persona = models.OneToOneField(persona, on_delete=models.CASCADE, primary_key=True)
    curriculum = models.FileField(null=True, blank=True)
    fecha_ingreso = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.fecha_ingreso


class alumno(models.Model):
    cuit_persona = models.OneToOneField(persona, on_delete=models.CASCADE)
    matricula_univ = models.AutoField(primary_key=True)

    def __str__(self):
        return self.matricula_univ


# class forma_pago(models.Model):
#     cod_forma_pago = models.AutoField(primary_key=True)
#     descrip_forma_pago = models.CharField(max_length=100)




class pago(models.Model):
    cuit_persona = models.OneToOneField(persona,on_delete=models.CASCADE)
    cod_curso = models.ForeignKey(curso,on_delete=models.CASCADE)
    fecha_pago = models.DateField(default=date.today)
    importe_pago = models.PositiveIntegerField(null=True)
    forma_pago = models.CharField(max_length=30, null=True)
    nro_recibo = models.AutoField(primary_key=True)



class universidad(models.Model):
    cod_universidad = models.PositiveIntegerField(primary_key=True)
    nombre_universidad = models.CharField(max_length=60)
    web = models.CharField(max_length=60)


class tipo_titulo(models.Model):
    cod_tipo_titulo = models.PositiveIntegerField(primary_key=True)
    des_tipo_titulo = models.CharField(max_length=30)


class titulo(models.Model):
    cod_titulo = models.PositiveIntegerField(primary_key=True)
    nombre_titulo = models.CharField(max_length=60)
    cod_universidad = models.ForeignKey('universidad', on_delete=models.PROTECT)
    cod_tipo_titulo = models.ForeignKey('tipo_titulo', on_delete=models.PROTECT)


class persona_titulo(models.Model):
    cod_titulo = models.ForeignKey('titulo', on_delete=models.PROTECT)
    cuil_persona = models.ForeignKey('persona', on_delete=models.PROTECT)
    fecha_titulo = models.DateField(null=True)

"""
class propuesta_curso(models.Model):
    cod_curso = models.OneToOneField('curso', on_delete=models.PROTECT, related_name='propuesta')
    cuil_persona = models.ForeignKey('persona', on_delete=models.PROTECT)
    fecha_propuesta_curso = models.DateField(null=True)
"""

class resolucion(models.Model):
    cod_curso = models.OneToOneField('curso', on_delete=models.PROTECT, related_name='resolucion')
    nro_resolucion = models.AutoField(primary_key=True)
    fecha_resolucion = models.DateField(default=date.today)

    def __str__(self):
        return "Resolucion {}".format(self.nro_resolucion)

class semana_curso(models.Model):
    cod_curso = models.ForeignKey('curso', on_delete=models.CASCADE, related_name='semanas')
    nro_semana = models.PositiveIntegerField(primary_key=True)
    objetivo_semanal = models.CharField(max_length=100, null=True)
    contenido_semanal = models.CharField(max_length=100, null=True)
