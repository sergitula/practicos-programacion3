from django import forms
from django.core.exceptions import ValidationError
from django.forms import DateInput, TextInput, Textarea
from apps.models import curso

class CursoForm(forms.ModelForm):
    class Meta:
        model = curso
        fields = ['nombre_curso', 'fecha_ini_curso', 'fecha_fin_curso', 'objetivo_general', 'modalidad_curso', 'duracion_curso', 'costo_curso', 'cant_minima_alumnos', 'cant_maxima_alumnos']

        widgets = {
            'fecha_proposicion': DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),
            'fecha_ini_curso': DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),
            'fecha_fin_curso': DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),
            'objetivo_general': Textarea(attrs={'row':5,'cols':40}),
        }

    def clean(self):
        cleaned_data = super().clean()
        fecha_inicio = self.cleaned_data['fecha_ini_curso']
        fecha_fin = self.cleaned_data['fecha_fin_curso']
        # Verifica que la fecha de inicio sea anterior a fecha fin.
        if fecha_fin and fecha_inicio > fecha_fin:
            raise ValidationError(
                {'fecha_ini_curso': 'La Fecha de Inicio no puede ser posterior que la fecha fin'},
                code='invalido'
            )
        return cleaned_data