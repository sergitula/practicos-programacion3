# from betterforms.multiform import MultiModelForm
from django import forms
from django.core.exceptions import ValidationError
from django.forms import DateInput
from apps.models import resolucion, curso

class EstadoCursoForm(forms.ModelForm):
    class Meta:
        model = curso
        fields = ['estado_curso']


class ResolucionForm(forms.ModelForm):
    class Meta:
        model = resolucion
        fields = []   #fields = ['cod_curso', 'nro_resolucion', 'fecha_resolucion']



        """
        widgets = {
            'fecha_resolucion': DateInput(format='%Y-%m-%d', attrs={'type': 'date'})
        }"""

    """
    def cambiarEstadoCurso(self):
        cleaned_data = super().clean()
        cursoRelacionado = curso.objects.get(cod_curso=self.fields[0])
    """
"""
class ResolucionCompleta(MultiModelForm):
    form_classes = {
        'eCurso': EstadoCursoForm,
        'resoluc': ResolucionForm,
    }
"""