
from django import forms
from django.core.exceptions import ValidationError
from django.forms import DateInput
from .models import persona, docente, alumno, curso_persona, pago


class IncripcionForm (forms.ModelForm):
    class Meta:
        model = persona
        fields = ('cuit_persona', 'apellido', 'nombre', 'sexo_persona', 'fecha_nac_persona', 'dom_calle', 'dom_nro_calle', 'estado_civil','dom_cod_localidad','dom_cod_provincia','departamento', 'correo_electronico','telefono')

        widgets = {
            'fecha_nac_persona': DateInput(format='%Y-%m-%d', attrs={'type':'date'}),
        }

class AlumnoForm (forms.ModelForm):
    class Meta:
        model = alumno
        fields = ('cuit_persona', 'matricula_univ')


class DocenteForm (forms.ModelForm):
    class Meta:
        model = docente
        fields = ('cuit_persona','curriculum', 'fecha_ingreso')

        def clean_requisitos(self):
            curriculum = self.cleaned_data['curriculum']
            if curriculum:
                extension = curriculum.name.rsplit('.', 1)[1].lower()
                if extension != 'pdf':
                    raise ValidationError('El archivo seleccionado no tiene el formato PDF.')
            return curriculum

class CursoPersonaForm (forms.ModelForm):
    class Meta:
        model = curso_persona
        fields = ('cuit_persona','cod_curso')


class abonadoForm (forms.ModelForm):
    class Meta:
        model = pago
        fields = ('cuit_persona','cod_curso','importe_pago','forma_pago')