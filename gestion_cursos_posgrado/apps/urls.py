from django.db.models import Index
from django.urls import path

from apps import views

urlpatterns =[
    path('index/', views.index, name = 'index'),
    path('index/<int:pk>/', views.inscribirPersonsaCurso, name='inscribirPersonsaCurso'),
    path('darDeBajaCursoPersona/<int:pk>', views.darDeBajaPersonsaCurso, name='darDeBajaCursoPersona'),
    path('abonarCurso/<int:pk>/', views.registrarAbonado, name='abonarCurso'),

    path('reciboDePago/<int:pk>,<str:pkC>/',views.reciboDePago, name='reciboDePago'),

    path('index/inicioSesion/', views.inicioSesion, name = 'inicioSesion'),

    path('administracionInscripcion/', views.administracionInscripcion, name='administracionInscripcion'),
    path('index/<int:pk>/nuevaInscripcion/', views.crearPersona, name='nuevaInscripcion'),
    path('editarInscripcion/<int:pk>',views.editarPersona, name='editarInscripcion'),
    path('eliminarInscripcion/<int:pk>',views.borrar_persona, name='eliminarInscripcion'),

    path('registroDocenteAlumno/', views.registroDocenteAlumno, name='registroDocenteAlumno'),

    # path('inscripcion/', views.crearPersona, name='inscripcionAlCurso'),
    # path('inscripcionAlCurso/', views.inscripcionAlCurso, name='inscripcionAlCurso'),
    #path('index/<int:pk>/inscripcionAlCurso', views.inscripcionAlCurso, name='inscripcionAlCurso'),

    path('administracionDeCursos/', views.paginaDelAdmin, name='administracionDeCursos'),
    #path('administracionDeCursos/creacionDeCurso', views.creacionDeCursos, name='creacionDeCurso'),
    path('administracionDeCursos/cursosDisponibles', views.cursosRegistrados, name='cursosDisponibles'),
    #path('administracionDeCursos/resolucion', views.nuevaResolucion, name='resolucion'),

    path('administracionDeCursos/estadisticas', views.pagEstadisticas, name='estadisticas'),
    path('administracionDeCursos/estadisticas/estadisticasFiltradas', views.pagEstadisticasFiltradas, name='estadisticasFiltradas'),

    path('administracionDeCursos/estadisticas/inscriptosACurso/<int:pk>', views.pagEstadisticasInscriptosACurso, name='incriptosACurso'),
    path('administracionDeCursos/estadisticas/pagosDelCurso/<int:pk>', views.pagEstadisticasPagoCurso, name='pagosDelCurso'),

    path('administracionDeCursos/estadisticas/inscriptosALosCursos', views.pagEstadisticasInscriptosACursos, name='incriptosALosCursos'),
    path('administracionDeCursos/estadisticas/inscriptosALosCursosFiltrados', views.pagEstadisticasInscriptosACursoFiltrados, name='incriptosALosCursosFiltrados'),

    path('administracionDeCursos/estadisticas/pagosEnCursos', views.pagEstadisticasPagoCursos, name='pagosEnCursos'),
    path('administracionDeCursos/estadisticas/pagosEnCursosFiltrados', views.pagEstadisticasPagoCursosFiltrados, name='pagosEnCursosFiltrados'),

    path('create/', views.nuevoCurso, name='nuevoCurso'),
    path('create/docentes/<int:pk>', views.escogerExpositores, name='escogerExpositores'),
    path('create/docentes/<int:pk>,<str:pkD>/agregar', views.escogerExpositores2, name='escogerExpositores2'),
    path('create/docentes/<int:pk>,<str:pkD>/quitar', views.quitarExpositores, name='escogerExpositores3'),
    path('edit/<int:pk>', views.curso_edit, name='curso_edit'),
    path('delete/', views.curso_delete, name='curso_delete'),

    path('create_resolucion/<int:pk>/', views.nuevaResolucion, name="create_resolucion"),

]
app_name = 'apps'
