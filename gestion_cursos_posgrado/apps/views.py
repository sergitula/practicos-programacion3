from functools import reduce

from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.forms import inlineformset_factory


from django.shortcuts import render

from django.views.generic import CreateView

from apps.models import curso, docente, resolucion, curso_persona, pago
from apps.formCurso import CursoForm
from apps.formResolucion import ResolucionForm, EstadoCursoForm

from apps.models import curso, docente
from apps.formCurso import CursoForm
from apps.formResolucion import ResolucionForm


from apps.models import curso, docente, persona, semana_curso, localidad, provincia
from apps.formCurso import CursoForm
from apps.forms import IncripcionForm, AlumnoForm, DocenteForm, CursoPersonaForm, abonadoForm


def inicio(request):
    return render(request,'inicio.html')

def index(request):
    return render(request,'inform-pago-cursos/index.html',{"curso":curso.objects.all()})

# def detalleDelCurso(request,pk):
#     cursoID = get_object_or_404(curso, pk=pk)
#     semanaCurso = get_object_or_404(semana_curso, pk=pk)
#     userprofile = User.objects.get(id=request.user.id)
#     pers = persona.objects.filter(User=userprofile)
#     return render(request,'inform-pago-cursos/detalleDelCurso.html',{"curso":cursoID,"semana":semanaCurso,"persona":pers})

def administracionInscripcion(request):
    return render(request,'inform-pago-cursos/administracionInscripcion.html',{'personas':persona.objects.all()})


def inicioSesion(request):
    return render(request,'admin-cursos/pag-inicio-sesion.html')

# def inscripcionAlCurso(request):
#     return render(request,'inform-pago-cursos/InscripcionAlCurso.html')

def paginaDelAdmin(request):
    return render(request,'admin-cursos/pag-admin.html')

def creacionDeCursos(request):
    return render(request,'admin-cursos/pag-crear-curso.html')


def cursosRegistrados(request):
    cursos = curso.objects.all()
    return render(request, 'admin-cursos/pag-tabla.html', {'cursos': cursos})


# @permission_required('apps.add_curso', raise_exception=True)
def nuevoCurso(request):
    nuevo_curso = None
    if request.method == 'POST':
        formulario_curso = CursoForm(request.POST, request.FILES)
        if formulario_curso.is_valid():
            nuevo_curso = formulario_curso.save(commit=True)
            messages.success(request,'Se ha agregado correctamente el Curso {}'.format(nuevo_curso))
            return redirect(reverse('apps:escogerExpositores', args={nuevo_curso.cod_curso}))
    else:
        formulario_curso = CursoForm()
    return render(request, 'admin-cursos/pag-crear-curso.html', {'form': formulario_curso})

# @permission_required('apps.change_curso', raise_exception=True)
def curso_edit(request, pk):
    cursoE = get_object_or_404(curso, pk=pk)
    if request.method == 'POST':
        form_curso = CursoForm(request.POST, request.FILES, instance=cursoE)
        if form_curso.is_valid():
            form_curso.save()
            messages.success(request, 'Se ha actualizado correctamente el curso')
            return redirect(reverse('apps:escogerExpositores', args={cursoE.cod_curso}))
            #return redirect(reverse('apps:cursosDisponibles'))
    else:
        form_curso = CursoForm(instance=cursoE)

    return render(request, 'admin-cursos/pag-edit-curso.html', {'form': form_curso})


def curso_delete(request):
    if request.method == 'POST':
        if 'cod_curso' in request.POST:
            cursoD = get_object_or_404(curso, pk=request.POST['cod_curso'])
            nombre_curso = cursoD.nombre_curso
            cursoD.delete()
            messages.success(request, 'Se ha eliminado exitosamente el Programa {}'.format(nombre_curso))
        else:
            messages.error(request, 'Debe indicar qué Programa se desea eliminar')
    return redirect(reverse('apps:cursosDisponibles'))


def curso_detalle(request, pk):
    cursoADetallar = get_object_or_404(curso, pk=pk)
    return render(request, 'inform-pago-cursos/detalleDelCurso.html', {'curso': cursoADetallar})

#@permission_required('apps.add_resolucion', raise_exception=True)
def nuevaResolucion(request, pk):
    cursoRelacionado = curso.objects.get(cod_curso=pk)
    if request.method == 'POST':
        formulario_estado = EstadoCursoForm(request.POST, request.FILES, instance=cursoRelacionado)

        if formulario_estado.is_valid:
            formulario_estado.save()
            resolucionN = resolucion(cod_curso=cursoRelacionado)
            resolucionN.save()
            messages.success(request, 'Se ha cargado correctamente la {}'.format(resolucionN))
            return redirect(reverse('apps:cursosDisponibles'))
    else:
        #formulario_resolucion = ResolucionForm()
        formulario_estado = EstadoCursoForm()
    return render(request, 'admin-cursos/pag-estado-resolucion.html', {'formEstado':formulario_estado, 'curso':cursoRelacionado})

#<<<<<<< HEAD
def pagEstadisticas(request):
    cursos = curso.objects.filter(estado_curso="A")
    return render(request, 'admin-cursos/pag-estadistica.html', {'cursos': cursos})

def pagEstadisticasFiltradas(request):
    if request.method == 'POST':
        if 'fecha_fin_curso' in request.POST:
            if 'fecha_ini_curso' in request.POST:
                cursos = curso.objects.filter(estado_curso="A",fecha_ini_curso__gte=request.POST['fecha_ini_curso'], fecha_fin_curso__lte=request.POST['fecha_fin_curso'])
    return render(request, 'admin-cursos/pag-estadistica.html', {'cursos': cursos})

def pagEstadisticasInscriptosACurso(request, pk):
    cursoE = curso.objects.get(cod_curso=pk)
    cursoInscripto = curso_persona.objects.filter(cod_curso=pk)
    return render(request, 'admin-cursos/pag-estadistica1.html', {'cursoE':cursoE,'cursoInscripto': cursoInscripto})

def pagEstadisticasPagoCurso(request, pk):
    cursoE = curso.objects.get(cod_curso=pk)
    cursoInscriptoP = curso_persona.objects.filter(cod_curso=pk, estado_de_pago=True)
    cursoInscriptoNoP = curso_persona.objects.filter(cod_curso=pk, estado_de_pago=False)
    return render(request, 'admin-cursos/pag-estadistica2.html', {'cursoE': cursoE, 'cursoInscriptoP': cursoInscriptoP, 'cursoInscriptoNoP': cursoInscriptoNoP})

def pagEstadisticasInscriptosACursos(request):
    cursos = curso.objects.filter(estado_curso="A")
    alumnosCursos = curso_persona.objects.order_by("cod_curso").all()
    return render(request, 'admin-cursos/pag-estadistica3.html', {'cursos': cursos, 'alumnosCursos': alumnosCursos})

def pagEstadisticasInscriptosACursoFiltrados(request):
    cursos = None
    alumnosCursos = None
    if request.method == 'POST':
        if 'fecha_fin_curso' in request.POST:
            if 'fecha_ini_curso' in request.POST:
                cursos = curso.objects.filter(estado_curso="A",fecha_ini_curso__gte=request.POST['fecha_ini_curso'],
                                              fecha_fin_curso__lte=request.POST['fecha_fin_curso'])
                alumnosCursos = curso_persona.objects.order_by("cod_curso").all()
    return render(request, 'admin-cursos/pag-estadistica3.html', {'cursos': cursos, 'alumnosCursos': alumnosCursos})

def pagEstadisticasPagoCursos(request):
    cursos = curso.objects.filter(estado_curso="A")
    alumnosCursos = curso_persona.objects.order_by("cod_curso").all()
    return render(request, 'admin-cursos/pag-estadistica4.html', {'cursos': cursos, 'alumnosCursos': alumnosCursos})

def pagEstadisticasPagoCursosFiltrados(request):
    cursos = None
    alumnosCursos = None
    if request.method == 'POST':
        if 'fecha_fin_curso' in request.POST:
            if 'fecha_ini_curso' in request.POST:
                cursos = curso.objects.filter(estado_curso="A",fecha_ini_curso__gte=request.POST['fecha_ini_curso'],
                                              fecha_fin_curso__lte=request.POST['fecha_fin_curso'])
                alumnosCursos = curso_persona.objects.order_by("cod_curso").all()
    return render(request, 'admin-cursos/pag-estadistica4.html', {'cursos': cursos, 'alumnosCursos': alumnosCursos})

def escogerExpositores(request, pk):
    cursoC = curso.objects.get(cod_curso=pk)
    docentes = docente.objects.all()
    expositores = cursoC.expositores.all()
    return render(request, 'admin-cursos/pag-agregar-docentes-curso.html', {'cursoC': cursoC, 'docentes':docentes, 'expositores':expositores})

def escogerExpositores2(request, pk, pkD):
    cursoC = curso.objects.get(cod_curso=pk)
    docenteE = docente.objects.get(cuit_persona=pkD)
    docentes = docente.objects.all()
    expositores = cursoC.expositores.all()
    if docenteE in expositores:
        messages.error(request,"El docente de cuil {} ya fue agregado anteriormente".format(docenteE.cuit_persona.cuit_persona))
    else:
        messages.success(request,"El docente  {} ha sido agregado".format(docenteE.cuit_persona.nombre))
        cursoC.expositores.add(docenteE)
        #cursoC.expositores.all().union(docenteEQS)

        expositores = cursoC.expositores.all()
        cursoC.save()
    return render(request, 'admin-cursos/pag-agregar-docentes-curso.html', {'cursoC': cursoC, 'docentes':docentes, 'expositores':expositores})

def quitarExpositores(request, pk, pkD):
    cursoC = curso.objects.get(cod_curso=pk)
    docenteQ = docente.objects.get(cuit_persona=pkD)
    docentes = docente.objects.all()
    cursoC.expositores.remove(docenteQ)
    cursoC.save()
    expositores = cursoC.expositores.all()
    messages.success(request,"El docente de cuil {} se ha quitado".format(docenteQ.cuit_persona.cuit_persona))

    return render(request, 'admin-cursos/pag-agregar-docentes-curso.html',
                  {'cursoC': cursoC, 'docentes': docentes, 'expositores': expositores})


@login_required
def crearPersona(request, pk):
    if request.method == 'POST':
        userprofile = User.objects.get(id=request.user.id)
        formulario_inscripcion = IncripcionForm(request.POST, request.FILES)
        if formulario_inscripcion.is_valid():
            nueva_persona = formulario_inscripcion.save()
            nueva_persona.User = userprofile
            nueva_persona.save()

            messages.success(request, 'Bienvenido/a {}, sus datos se guardaron exitosamente.'.format(nueva_persona))
            # return redirect(reverse('apps:index'))
    else:
        formulario_inscripcion = IncripcionForm()
    return render(request, 'inform-pago-cursos/InscripcionAlCurso.html',{
        'form': formulario_inscripcion,
        'localidad': localidad.objects.all(),
        'provincia':provincia.objects.all(),
        'persona': provincia.objects.all(),
    })




@login_required
def registroDocenteAlumno(request):
    userprofile = User.objects.get(id=request.user.id)
    pers = persona.objects.filter(User=userprofile)
    formulario_inscripcion_docente = DocenteForm(request.POST, request.FILES)
    if request.method == 'POST':
        if formulario_inscripcion_docente.is_valid():
            docente = formulario_inscripcion_docente.save()
            docente.save()
            messages.success(request, 'Los datos del docente se guardaron con éxito.')
            # return redirect(reverse('apps:index'))
    else:
        formulario_inscripcion_docente = DocenteForm()
    return render(request, 'inform-pago-cursos/inscripcionAdicional.html',{
        'form':formulario_inscripcion_docente,
        'persona':pers,
    })


@login_required
def inscribirPersonsaCurso(request,pk):
    cursoID = get_object_or_404(curso, pk=pk)
    # semanaCurso = get_object_or_404(semana_curso, pk=pk)

    userprofile = User.objects.get(id=request.user.id)
    pers = persona.objects.filter(User=userprofile)

    totalInscriptosALCurso = curso_persona.objects.filter(cod_curso=pk)

    formulario_cursoPersona = CursoPersonaForm(request.POST)
    if request.method == 'POST':
        if formulario_cursoPersona.is_valid():
            cursoPersona = formulario_cursoPersona.save()
            cursoPersona.save()

            messages.success(request, 'Los datos se guardaron con éxito.')
            return redirect(reverse('apps:index'))
    else:
        formulario_cursoPersona = CursoPersonaForm()
    return render(request, 'inform-pago-cursos/detalleDelCurso.html',{
        'form':formulario_cursoPersona,
        'persona':pers,
        "personaRegistrada": len(pers),
        "curso": cursoID,
        # "semana": semanaCurso,
        "cursoPersona": curso_persona.objects.all(),
        "totalInscriptos": len(totalInscriptosALCurso),
    })


def darDeBajaPersonsaCurso(request,pk):
    personaCurso = get_object_or_404(curso_persona, pk=pk)
    if request.method == 'POST':
            # nombre_persona = personaCurso.nombre
            personaCurso.delete()
            messages.success(request, 'Se ha dado de baja al curso')
            return redirect(reverse('apps:index'))
    return render(request, 'inform-pago-cursos/darDeBajaCursoPersona.html', {'personaCurso': personaCurso})




@login_required
def editarPersona(request, pk):
    # userprofile = User.objects.get(id=request.user.id)
    personaID = get_object_or_404(persona, pk=pk)
    if request.method == 'POST':
        form_persona = IncripcionForm(request.POST, request.FILES, instance=personaID)
        if form_persona.is_valid():
            form_persona.save()
            messages.success(request, 'Se ha actualizado correctamente los datos personales.')
            return redirect(reverse('apps:index'))
    else:
        form_persona = IncripcionForm(instance=personaID)
    return render(request, 'inform-pago-cursos/inscripcion_edit.html', {'form': form_persona,
                                                                          'personas':persona.objects.all(),
                                                                        'provincia':provincia.objects.all()
                                                                        })






def borrar_persona(request,pk):
    personaID = get_object_or_404(persona, pk=pk)
    if request.method == 'POST':
            nombre_persona = personaID.nombre
            personaID.delete()
            messages.success(request, 'Se han borrado todos los datos de {}'.format(nombre_persona))
            return redirect(reverse('apps:index'))
    return render(request, 'inform-pago-cursos/inscripcion_delete.html', {'persona': personaID})




#
# def abonarCurso(request, pk):
#     cursoID = get_object_or_404(curso, pk=pk)
#
#     userprofile = User.objects.get(id=request.user.id)
#     pers = persona.objects.filter(User=userprofile)
#
#     return render(request, 'inform-pago-cursos/pagoDelCurso.html', {'curso':cursoID,
#                                                                     'persona':pers,})


def registrarAbonado(request, pk):
    cursoID = get_object_or_404(curso, pk=pk)

    userprofile = User.objects.get(id=request.user.id)
    pers = persona.objects.filter(User=userprofile)


    if request.method == 'POST':
        formularioDeAbonado = abonadoForm(request.POST, request.FILES)


        if formularioDeAbonado.is_valid():
            # cuit_persona = request.POST['cuit_persona']
            abonado = formularioDeAbonado.save()
            abonado.save()


            cuit_persona = request.POST['cuit_persona']
            cursoPersona = (curso_persona.objects.filter(cuit_persona_id=cuit_persona).filter(
                cod_curso_id=cursoID.cod_curso)).values_list('nro_registro',flat=True)
            query = reduce(curso_persona.nro_registro, (Q(nro_registro=id) for id in cursoPersona))
            curPer = get_object_or_404(curso_persona,query)
            curPer.estado_de_pago = True
            curPer.save()


            messages.success(request, 'Se ha abonado correctamente el curso.')
            # return redirect(reverse('apps:reciboDePago', args={cuit_persona, cursoID.cod_curso}))
    else:
        formularioDeAbonado= abonadoForm()
    return render(request, 'inform-pago-cursos/pagoDelCurso.html',{'form': formularioDeAbonado,
                                                                   'curso': cursoID,
                                                                   'persona': pers,
                                                                   'curPer': curso_persona.objects.all(),
                                                                   })


def reciboDePago(request,pk, pkC):
    recibo = pago.objects.filter(cuit_persona_id= pk).filter(cod_curso_id=pkC)
    return render(request,'inform-pago-cursos/cuponPago.html', {'recibo':recibo,
                                                                })




