from django import forms
from django.core.exceptions import ValidationError
from django.forms import DateInput
from apps.models import docente

class DocenteForm(forms.ModelForm):
    class Meta:
        model = docente
        fields = ["curriculum"]
